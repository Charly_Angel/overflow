if (process.env.NODE_ENV !== "production") {
  require("dotenv").config();
}

module.exports = {
  URI_FIREBASE: process.env.URI_FIREBASE,
  HOST: process.env.HOST || "localhost",
  PORT: process.env.PORT,
  NODE_ENV: process.env.NODE_ENV || "dev",
  TIME_TO_LIVE: 1000 * 60 * 60 * 24 * process.env.TIME_TO_LIVE_DAYS,
};
