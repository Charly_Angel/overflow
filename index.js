"use strict";
const { PORT, HOST, NODE_ENV, TIME_TO_LIVE } = require("./config/config");
const Hapi = require("@hapi/hapi");
const handlebars = require("./lib/helpers");
const { methods } = require("./lib");
const vision = require("@hapi/vision");
const inert = require("@hapi/inert");
const path = require("path");
const routes = require("./routes");
const { SiteController } = require("./controllers");
const good = require("@hapi/good");
const crumb = require("@hapi/crumb");
const blankie = require("blankie");
const scooter = require("@hapi/scooter");
const hapiDevErrors = require("hapi-dev-errors");

//configuración del servidor
const server = Hapi.server({
  port: PORT,
  host: HOST,
  routes: {
    files: {
      relativeTo: path.join(__dirname, "public"),
    },
  },
});

async function init() {
  try {
    //asignar plugins a hapi
    await server.register(inert);
    await server.register(vision);
    await server.register({
      plugin: good,
      options: {
        reporters: {
          console: [
            {
              module: "@hapi/good-console",
            },
            "stdout",
          ],
        },
      },
    });

    await server.register({
      plugin: crumb,
      options: {
        cookieOptions: {
          isSecure: NODE_ENV === "prod",
        },
      },
    });

    await server.register([
      scooter,
      {
        plugin: blankie,
        options: {
          defaultSrc: `'self' 'unsafe-inline'`,
          styleSrc: `'self' 'unsafe-inline' https://maxcdn.bootstrapcdn.com`,
          fontSrc: `'self' 'unsafe-inline' data:`,
          scriptSrc: `'self' 'unsafe-inline' https://cdnjs.cloudflare.com https://maxcdn.bootstrapcdn.com/ https://code.jquery.com/`,
          generateNonces: false,
        },
      },
    ]);

    await server.register({
      plugin: hapiDevErrors,
      options: {
        showErrors: NODE_ENV !== "prod",
      },
    });

    await server.register({
      plugin: require("./lib/api"),
      options: {
        prefix: "api",
      },
    });

    server.method("setAnswerRight", methods.setAnswerRight);
    server.method("getLast", methods.getLast, {
      cache: {
        expiresIn: 100 * 60,
        generateTimeout: 2000,
      },
    });

    server.state("user", {
      ttl: TIME_TO_LIVE,
      isSecure: NODE_ENV === "prod",
      encoding: "base64json",
    });

    server.views({
      engines: {
        hbs: handlebars,
      },
      relativeTo: __dirname,
      path: "views",
      layout: true,
      layoutPath: "views",
    });

    server.ext("onPreResponse", SiteController.fileNotFound);
    //rutas
    server.route(routes);
    //iniciar servidor
    await server.start();
  } catch (error) {
    console.error(error.message);
    process.exit(1);
  }
  server.log("info", `servidor lanzado en ${server.info.uri}`);
}

//manejo de errores
process.on("unhandledRejection", (error) =>
  server.log("UnhandledRejection", error)
);

process.on("uncaughtException", (error) =>
  server.log("UncaughtException", error)
);

init();
