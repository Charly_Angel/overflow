"use strict";
const {
  SiteController,
  UserController,
  QuestionController,
} = require("./controllers");
const Joi = require("@hapi/joi");
module.exports = [
  {
    method: "GET",
    path: "/",
    options: {
      cache: {
        expiresIn: 1000 * 30,
        privacy: "private",
      },
    },
    handler: SiteController.home,
  },
  {
    method: "GET",
    path: "/register",
    handler: SiteController.register,
  },
  {
    method: "GET",
    path: "/login",
    handler: SiteController.login,
  },
  {
    method: "GET",
    path: "/logout",
    handler: UserController.logout,
  },
  {
    method: "GET",
    path: "/ask",
    handler: SiteController.ask,
  },

  {
    method: "GET",
    path: "/question/{id}",
    handler: SiteController.viewQuestion,
  },
  {
    method: "POST",
    path: "/create-user",
    options: {
      validate: {
        payload: Joi.object({
          name: Joi.string().required().min(3),
          email: Joi.string().email().required(),
          password: Joi.string().required().min(6),
        }),
        failAction: UserController.failValidation,
      },
    },
    handler: UserController.createUser,
  },
  {
    method: "POST",
    path: "/validate-user",
    options: {
      validate: {
        payload: Joi.object({
          email: Joi.string().email().required(),
          password: Joi.string().required().min(6),
        }),
        failAction: UserController.failValidation,
      },
    },
    handler: UserController.validateUser,
  },
  {
    method: "POST",
    path: "/create-question",
    options: {
      payload: {
        parse: true,
        multipart: true,
      },
      validate: {
        payload: Joi.object({
          title: Joi.string().required(),
          description: Joi.string().required(),
          image: Joi.any().optional(),
        }),
        failAction: UserController.failValidation,
      },
    },
    handler: QuestionController.createQuestion,
  },
  {
    method: "POST",
    path: "/answer-question",
    options: {
      validate: {
        payload: Joi.object({
          answer: Joi.string().required(),
          id: Joi.string().required(),
        }),
        failAction: UserController.failValidation,
      },
    },
    handler: QuestionController.answerQuestion,
  },
  {
    method: "GET",
    path: "/answer/{questionId}/{answerId}",
    handler: QuestionController.setAnswerRight,
  },
  {
    method: "GET",
    path: "/assets/{param*}",
    handler: {
      directory: {
        path: ".",
        index: ["index.html"],
      },
    },
  },
  {
    method: ["GET", "POST"],
    path: "/{any*}",
    handler: SiteController.notFound,
  },
];
