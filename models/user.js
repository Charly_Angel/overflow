"use strict";
const bcrypt = require("bcrypt");

class User {
  constructor(db) {
    this.db = db;
    this.ref = this.db.ref("/");
    this.collection = this.ref.child("users");
  }

  async create(data) {
    const user = { ...data };
    user.password = await this.constructor.encrypt(user.password);
    const newUser = this.collection.push();
    newUser.set(user);
    return newUser.key;
  }

  async validate(data) {
    const user = { ...data };
    const useQuery = await this.collection
      .orderByChild("email")
      .equalTo(user.email)
      .once("value");
    const userFound = useQuery.val();

    if (!userFound) {
      return false;
    }

    const userId = Object.keys(userFound)[0];
    const passwordRight = await bcrypt.compare(
      user.password,
      userFound[userId].password
    );
    const result = passwordRight ? userFound[userId] : false;
    return result;
  }

  static async encrypt(password) {
    const salRounds = 10;
    const hashedPassword = await bcrypt.hash(password, salRounds);
    return hashedPassword;
  }
}

module.exports = User;
