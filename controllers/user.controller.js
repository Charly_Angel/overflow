"use strict";

const { User } = require("../models");
const Boom = require("@hapi/boom");

async function createUser(req, h) {
  let result;
  try {
    result = await User.create(req.payload);
  } catch (error) {
    req.error("error", error.message);
    return h.view("register", {
      title: "Register",
      error: "Error creando el usuario",
    });
  }
  req.log("info", result);
  return h.view("register", {
    title: "Register",
    success: "Usuario creado exitosamente",
  });
}

async function validateUser(req, h) {
  let result;

  try {
    result = await User.validate(req.payload);
    if (!result) {
      req.log("error", `Email y/o contraseña inválidos ${req.payload.email}`);
      return h.view("login", {
        title: "Login",
        error: "Email y/o contraseña inválidos",
      });
    }
  } catch (error) {
    req.error(error.message);
    return h.view("login", {
      title: "Login",
      error: "Internal error",
    });
  }

  return h
    .redirect("/")
    .state("user", { name: result.name, email: result.email });
}

function logout(req, h) {
  return h.redirect("/login").unstate("user");
}

function failValidation(req, h, error) {
  const templates = {
    "/create-user": "register",
    "/validate-user": "login",
    "/create-question": "ask",
  };
  return h
    .view(templates[req.path], {
      title: "Error de validación",
      error: "Favor de completar los campos requeridos",
    })
    .code(400)
    .takeover();
}

module.exports = {
  createUser,
  logout,
  validateUser,
  failValidation,
};
